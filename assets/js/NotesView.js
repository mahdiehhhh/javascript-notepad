export default class NotesView {
    constructor(root, handlers) {
        this.root = root;
        const {
            onNoteSelect,
            onNoteDelete,
            onNoteAdd,
            onNoteEdit
        } = handlers;

        this.onNoteSelect = onNoteSelect;

        this.onNoteEdit = onNoteEdit;

        this.onNoteDelete = onNoteDelete;

        this.onNoteAdd = onNoteAdd;

        this.root.innerHTML = `
            <div class="notes__sidebar">
                <h2 class="notes__logo">Note Application</h2>

                <ul class="notes__list"></ul>

                <button class="notes__sidebar-add-btn">Add Note</button>
            </div>

            <div class="notes__preview">
                <input type="text" class="notes__preview-title" placeholder="Note Title">

                <textarea class="notes__preview-body" placeholder="Add your note"></textarea>

                <button class="notes__preview-save-btn">Save </button>
            </div>
        `
        const addNoteBtn = document.querySelector('.notes__sidebar-add-btn');

        const notesSaveBtn = document.querySelector('.notes__preview-save-btn');

        const notesTitle = document.querySelector('.notes__preview-title');

        const notesBody = document.querySelector('.notes__preview-body');

        addNoteBtn.addEventListener('click', () => {
            this.onNoteAdd();
            // console.log('hi')
        });

        notesSaveBtn.addEventListener('click', () => {
            const newNoteTitle = notesTitle.value.trim();

            const newNotebody = notesBody.value.trim();

            this.onNoteEdit(newNoteTitle, newNotebody);
        });

        this.updateNotePreviewVisibility(false)
    }

    _createListItem(id, title, body, updated) {
        const MAX_LENGTH_BODY = 30;

        return `
            <li class="notes__list-item" data-note-id ="${id}">
            
                <div class="notes__list-item-title">${title}
                <span class="trash__span" data-note-id="${id}">
                    <i class="fa fa-trash-alt trash"></i>
                </span>
                </div>
                <span class="notes__list-item-description">
                    ${body.substring(0, MAX_LENGTH_BODY)}
                    ${body.length > MAX_LENGTH_BODY ? "..." : "" }
                </span>
                
                <span class="notes__list-item-update">
                    ${new Intl.DateTimeFormat('en', { dateStyle: "medium", timeStyle: "medium" }).format(new Date(updated)) }


                    
                </span>
            </li>
            <hr class="notes__list-item-hr" />
        `
    }

    updateNoteList(notes) {
        const noteContainer = this.root.querySelector('.notes__list');

        noteContainer.innerHTML = '';

        let notesList = '';

        for (const note of notes) {
            const {
                id,
                title,
                body,
                updated
            } = note;

            const html = this._createListItem(id, title, body, updated);

            notesList += html;
        }

        noteContainer.innerHTML = notesList;

        noteContainer.querySelectorAll('.notes__list-item').forEach((nodeItem) => {
            nodeItem.addEventListener('click', () => {
                this.onNoteSelect(nodeItem.dataset.noteId)
            })
        });

        noteContainer.querySelectorAll('.trash__span').forEach((nodeItem) => {
            nodeItem.addEventListener('click', (e) => {
                e.stopPropagation();
                this.onNoteDelete(nodeItem.dataset.noteId);
            })
        })
    }

    updateActiveNote(note) {
        this.root.querySelector('.notes__preview-title').value = note.title;

        this.root.querySelector('.notes__preview-body').value = note.body;

        this.root.querySelectorAll('.notes__list-item').forEach((item) => {
            item.classList.remove('notes__list-item-selected');
        })

        this.root.querySelector(`.notes__list-item[data-note-id="${note.id}"]`).classList.add('notes__list-item-selected')
    }

    updateNotePreviewVisibility(visible) {
        this.root.querySelector('.notes__preview').style.visibility = visible ? 'visible' : 'hidden';
    }
}