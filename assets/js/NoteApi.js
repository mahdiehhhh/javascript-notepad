const notes = [
  {
    id: 1,
    title: "first note",
    body: "some dummy text first",
    updated: "2021-10-31T15:02:00.411Z",
  },
  {
    id: 2,
    title: "second note",
    body: "some dummy text second",
    updated: "2021-10-31T15:03:23.556Z",
  },
  {
    id: 3,
    title: "third note",
    body: "this is third note",
    updated: "2021-11-01T10:47:26.889Z",
  },
];

export default class NoteApi {
    static getAllNotes() {
        const savedNotes = JSON.parse(localStorage.getItem("note-app")) || [];
        return savedNotes.sort((a, b) => {
          return new Date(a.updated) > new Date(b.updated) ? -1 : 1;
        });
    }

    static noteSave(saveToNote) {
        // 1.exist or 2.not

        const notes = NoteApi.getAllNotes();

        const existedNote = notes.find((n) => n.id === parseInt(saveToNote.id));

        if (existedNote) {
            existedNote.title = saveToNote.title;
            existedNote.body = saveToNote.body;
            existedNote.updated = new Date().toISOString();

        } else {
            saveToNote.id = new Date().getTime();
            saveToNote.updated = new Date().toISOString();
            notes.push(saveToNote)
        }

        localStorage.setItem('note-app', JSON.stringify(notes));
    }

    static deleteNote(id) {
        const notes = NoteApi.getAllNotes();
        const filteredNotes = notes.filter((n) => n.id != id);
        console.log('filter',filteredNotes)
        localStorage.setItem('note-app', JSON.stringify(filteredNotes))
    }

}
